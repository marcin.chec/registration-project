package specialists.registration.security;

import lombok.AllArgsConstructor;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import specialists.registration.domain.client.Client;
import specialists.registration.repository.ClientRepository;

import java.util.Collections;

@Service
@AllArgsConstructor
public class ClientDetailsServiceImpl implements UserDetailsService {

    private ClientRepository clientRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        return clientRepository.findByUsername(username)
                .map(this::toSecureUser)
                .orElseThrow(() -> new UsernameNotFoundException("User doesnt exist"));
    }

    private SecurityClient toSecureUser(Client user) {
        return new SecurityClient(
                user.getUsername(),
                user.getPassword(),
                Collections.singletonList(new SimpleGrantedAuthority("ROLE_" + user.getPermissionGroup()))
        );
    }
}
