package specialists.registration.security;

public enum PermissionGroup {
    USER, ADMIN
}
