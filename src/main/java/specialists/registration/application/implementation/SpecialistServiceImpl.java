package specialists.registration.application.implementation;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import specialists.registration.application.api.SpecialistService;
import specialists.registration.common.SpecialistToDtoConverter;
import specialists.registration.domain.specialist.Specialist;
import specialists.registration.domain.specialist.dto.AddedSpecialistDTO;
import specialists.registration.domain.specialist.dto.SpecialistDTO;
import specialists.registration.domain.specialist.dto.UpdatedSpecialistDTO;
import specialists.registration.repository.SpecialistRepository;

import javax.persistence.EntityNotFoundException;
import javax.transaction.Transactional;
import java.util.Optional;

@Service
@Transactional
@AllArgsConstructor
public class SpecialistServiceImpl implements SpecialistService {

    private SpecialistRepository specialistRepository;

    @Override
    public AddedSpecialistDTO addSpecialist(SpecialistDTO newSpecialist) {
        Specialist specialist = new Specialist(newSpecialist.firstName, newSpecialist.lastName, newSpecialist.specialization);
        specialistRepository.saveAndFlush(specialist);

        return SpecialistToDtoConverter.convertSpecialistToAddedSpecialistDTO(specialist);
    }

    @Override
    public UpdatedSpecialistDTO updateSpecialist(Long id, SpecialistDTO updatedSpecialist) {
        Optional<Specialist> specialist = specialistRepository.findById(id);
        if (!specialist.isPresent()) {
            throw new EntityNotFoundException("Specialist with id: '" + id + "' doesnt exists");
        }
        Specialist specialistEntity = specialist.get();

        specialistEntity.setFirstName(updatedSpecialist.firstName);
        specialistEntity.setLastName(updatedSpecialist.lastName);
        specialistEntity.setSpecialization(updatedSpecialist.specialization);

        return SpecialistToDtoConverter.convertSpecialistToUpdatedSpecialistDTO(specialistEntity);
    }

    @Override
    public void removeSpecialist(Long id) {
        Optional<Specialist> specialist = specialistRepository.findById(id);
        if (!specialist.isPresent()) {
            throw new EntityNotFoundException("Specialist with id: '" + id + "' doesnt exists");
        }
        specialistRepository.delete(specialist.get());
    }

    @Override
    public SpecialistDTO getSpecialist(Long id) {
        Optional<Specialist> specialist = specialistRepository.findById(id);
        if (!specialist.isPresent()) {
            throw new EntityNotFoundException("Specialist with id: '" + id + "' doesnt exists");
        }

        return SpecialistToDtoConverter.convertSpecialistToSpecialistDTO(specialist.get());
    }
}
