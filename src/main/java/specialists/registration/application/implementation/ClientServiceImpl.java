package specialists.registration.application.implementation;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import specialists.registration.application.api.ClientService;
import specialists.registration.common.ClientToDtoConverter;
import specialists.registration.common.exception.EntityAlreadyExistsException;
import specialists.registration.domain.client.Client;
import specialists.registration.domain.client.dto.AddedClientDTO;
import specialists.registration.domain.client.dto.GetClientDTO;
import specialists.registration.domain.client.dto.NewClientDTO;
import specialists.registration.domain.client.dto.UpdatedClientDTO;
import specialists.registration.repository.ClientRepository;
import specialists.registration.security.PermissionGroup;

import javax.persistence.EntityNotFoundException;
import javax.transaction.Transactional;
import java.util.Optional;

@Service
@Transactional
@NoArgsConstructor
@AllArgsConstructor
public class ClientServiceImpl implements ClientService {

    @Autowired
    private ClientRepository clientRepository;
    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Override
    public AddedClientDTO registerClient(NewClientDTO newClient) {
        if (clientRepository.existsByUsername(newClient.username)) {
            throw new EntityAlreadyExistsException("User with username: '" + newClient.username + "' already exists");
        }
        Client newCli = new Client(
                newClient.username,
                bCryptPasswordEncoder.encode(newClient.password),
                PermissionGroup.USER.name(),
                newClient.firstName,
                newClient.lastName,
                newClient.email
        );

        clientRepository.saveAndFlush(newCli);

        return ClientToDtoConverter.convertClientToAddedClientDTO(newCli);
    }

    @Override
    public UpdatedClientDTO updateClient(Long id, NewClientDTO updatedClient) {

        Optional<Client> client = clientRepository.findById(id);

        if (!client.isPresent()) {
            throw new EntityNotFoundException("User with id: '" + id + "' doesnt exists");
        }

        Client clientEntity = client.get();

        clientEntity.setFirstName(updatedClient.firstName);
        clientEntity.setLastName(updatedClient.lastName);
        clientEntity.setUsername(updatedClient.username);
        clientEntity.setEmail(updatedClient.email);
        clientEntity.setPassword(bCryptPasswordEncoder.encode(updatedClient.password));

        return ClientToDtoConverter.convertClientToUpdatedClientDTO(clientEntity);

    }

    @Override
    public void removeClient(Long id) {
        Optional<Client> client = clientRepository.findById(id);
        if (!client.isPresent()) {
            throw new EntityNotFoundException("User with id: '" + id + "' doesnt exists");
        }
        clientRepository.delete(client.get());
    }

    @Override
    public GetClientDTO getClient(Long id) {
        Optional<Client> client = clientRepository.findById(id);

        if (!client.isPresent()) {
            throw new EntityNotFoundException("User with id: '" + id + "' doesnt exists");
        }

        return ClientToDtoConverter.convertClientToDto(client.get());
    }

    @Override
    public GetClientDTO getLoggedInClient() {
        String login = (String) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        Optional<Client> client = clientRepository.findByUsername(login);

        if (!client.isPresent()) {
            throw new EntityNotFoundException("User with login: '" + login + "' doesnt exists");
        }
        return ClientToDtoConverter.convertClientToDto(client.get());
    }

}
