package specialists.registration.application.implementation;

import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import specialists.registration.application.api.RegistrationService;
import specialists.registration.common.RegistrationToDtoConverter;
import specialists.registration.common.exception.ChoosenDateNotAvailableException;
import specialists.registration.common.exception.ForeignRegistrationException;
import specialists.registration.domain.client.Client;
import specialists.registration.domain.registration.Registration;
import specialists.registration.domain.registration.dto.ChangedRegistrationDTO;
import specialists.registration.domain.registration.dto.MakedRegistrationDTO;
import specialists.registration.domain.registration.dto.NewRegistrationDTO;
import specialists.registration.repository.ClientRepository;
import specialists.registration.repository.RegistrationRepository;
import specialists.registration.repository.SpecialistRepository;

import javax.persistence.EntityNotFoundException;
import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class RegistrationServiceImpl implements RegistrationService {

    private static final Long MINUTES_OF_APPOINTMENT = 20L;
    private RegistrationRepository registrationRepository;
    private ClientRepository clientRepository;
    private SpecialistRepository specialistRepository;

    public RegistrationServiceImpl(RegistrationRepository registrationRepository, ClientRepository clientRepository, SpecialistRepository specialistRepository) {
        this.registrationRepository = registrationRepository;
        this.clientRepository = clientRepository;
        this.specialistRepository = specialistRepository;
    }

    @Override
    public MakedRegistrationDTO doRegistration(NewRegistrationDTO newRegistrationDTO) {

        Client loggedClient = getLoggedClient();

        checkDoesSpecialistExists(newRegistrationDTO.specialistId);

        if (!isAvailableByDate(newRegistrationDTO)) {
            throw new ChoosenDateNotAvailableException("Choosen date is not available");
        }

        Registration registration =
                new Registration(
                        loggedClient.getId(),
                        newRegistrationDTO.specialistId,
                        newRegistrationDTO.startOfAppointment,
                        newRegistrationDTO.startOfAppointment.plus(MINUTES_OF_APPOINTMENT, ChronoUnit.MINUTES)
                );

        registrationRepository.saveAndFlush(registration);

        return RegistrationToDtoConverter.convertToMakedRegistrationDTO(registration);
    }

    @Override
    public void resignFromRegistration(Long id) {

        Client loggedClient = getLoggedClient();

        Registration registrationToDelete = getRegistration(id);

        if (!registrationToDelete.getClientId().equals(loggedClient.getId())) {
            throw new ForeignRegistrationException("This is not your registration");
        }

        registrationRepository.delete(registrationToDelete);
    }

    @Override
    public ChangedRegistrationDTO changeRegistration(Long id, NewRegistrationDTO newRegistrationDTO) {

        Client loggedClient = getLoggedClient();

        Registration registrationToChange = getRegistration(id);

        checkDoesSpecialistExists(newRegistrationDTO.specialistId);

        if (!registrationToChange.getClientId().equals(loggedClient.getId())) {
            throw new ForeignRegistrationException("This is not your registration");
        }

        if (!isAvailableByDate(newRegistrationDTO)) {
            throw new ChoosenDateNotAvailableException("Choosen date is not available");
        }

        registrationToChange.setSpecialistId(newRegistrationDTO.specialistId);
        registrationToChange.setStartOfAppointment(newRegistrationDTO.startOfAppointment);
        registrationToChange.setEndOfAppointment(newRegistrationDTO.startOfAppointment.plus(MINUTES_OF_APPOINTMENT, ChronoUnit.MINUTES));

        return RegistrationToDtoConverter.convertToChangedRegistrationDTO(registrationToChange);
    }

    @Override
    public MakedRegistrationDTO showRegistration(Long id) {

        Registration registration = getRegistration(id);

        Client loggedClient = getLoggedClient();

        if (!registration.getClientId().equals(loggedClient.getId())) {
            throw new ForeignRegistrationException("This is not your registration");
        }
        return RegistrationToDtoConverter.convertToMakedRegistrationDTO(registration);
    }

    private boolean isAvailableByDate(NewRegistrationDTO newRegistrationDTO) {

        List<Registration> allRegistartions = registrationRepository.findAll();
        LocalDateTime endOfAppointment = newRegistrationDTO.startOfAppointment.plus(MINUTES_OF_APPOINTMENT, ChronoUnit.MINUTES);

        for (Registration registration : allRegistartions) {

            if (registration.getSpecialistId().equals(newRegistrationDTO.specialistId)) {

                if ((newRegistrationDTO.startOfAppointment.compareTo(registration.getStartOfAppointment()) >= 0)
                        && (newRegistrationDTO.startOfAppointment.compareTo(registration.getEndOfAppointment()) <= 0)) {
                    return false;
                }
                if ((endOfAppointment.compareTo(registration.getStartOfAppointment()) >= 0)
                        && (endOfAppointment.compareTo(registration.getEndOfAppointment()) <= 0)) {
                    return false;
                }

            }
        }
        return true;
    }

    private Client getLoggedClient() {

        String login = (String) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Optional<Client> loggedClient = clientRepository.findByUsername(login);

        if (!loggedClient.isPresent()) {
            throw new EntityNotFoundException("User with login: '" + login + "' doesnt exists");
        }

        return loggedClient.get();
    }

    private void checkDoesSpecialistExists(Long id) {

        if (!specialistRepository.existsById(id)) {
            throw new EntityNotFoundException("Specialist with id: '" + id + "' doesnt exists");
        }

    }

    private Registration getRegistration(Long id) {

        Optional<Registration> registration = registrationRepository.findById(id);

        if (!registration.isPresent()) {
            throw new EntityNotFoundException("Registration with id: '" + id + "' doesnt exists");
        }

        return registration.get();
    }

}
