package specialists.registration.application.api;


import specialists.registration.domain.registration.dto.ChangedRegistrationDTO;
import specialists.registration.domain.registration.dto.MakedRegistrationDTO;
import specialists.registration.domain.registration.dto.NewRegistrationDTO;

public interface RegistrationService {

    MakedRegistrationDTO doRegistration(NewRegistrationDTO newRegistrationDTO);

    void resignFromRegistration(Long id);

    ChangedRegistrationDTO changeRegistration(Long id, NewRegistrationDTO newRegistrationDTO);

    MakedRegistrationDTO showRegistration(Long id);
}
