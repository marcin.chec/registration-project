package specialists.registration.application.api;

import specialists.registration.domain.client.dto.AddedClientDTO;
import specialists.registration.domain.client.dto.GetClientDTO;
import specialists.registration.domain.client.dto.NewClientDTO;
import specialists.registration.domain.client.dto.UpdatedClientDTO;

public interface ClientService {
    AddedClientDTO registerClient(NewClientDTO newClient);

    UpdatedClientDTO updateClient(Long id, NewClientDTO updatedClient);

    void removeClient(Long id);

    GetClientDTO getClient(Long id);

    GetClientDTO getLoggedInClient();
}
