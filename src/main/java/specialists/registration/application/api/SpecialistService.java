package specialists.registration.application.api;

import specialists.registration.domain.specialist.dto.AddedSpecialistDTO;
import specialists.registration.domain.specialist.dto.SpecialistDTO;
import specialists.registration.domain.specialist.dto.UpdatedSpecialistDTO;

public interface SpecialistService {
    AddedSpecialistDTO addSpecialist(SpecialistDTO newSpecialist);

    UpdatedSpecialistDTO updateSpecialist(Long id, SpecialistDTO updatedSpecialist);

    void removeSpecialist(Long id);

    SpecialistDTO getSpecialist(Long id);
}
