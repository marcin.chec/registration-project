package specialists.registration.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import specialists.registration.domain.registration.Registration;

import java.util.List;
import java.util.Optional;

public interface RegistrationRepository extends JpaRepository<Registration, Long> {
    List<Registration> findAll();

    Optional<Registration> findById(Long id);
}
