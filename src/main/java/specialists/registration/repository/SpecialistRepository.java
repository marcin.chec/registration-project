package specialists.registration.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import specialists.registration.domain.specialist.Specialist;

import java.util.Optional;

public interface SpecialistRepository extends JpaRepository<Specialist, Long> {
    Optional<Specialist> findById(Long id);

    boolean existsById(Long id);
}
