package specialists.registration.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import specialists.registration.domain.client.Client;

import java.util.Optional;

public interface ClientRepository extends JpaRepository<Client, Long> {
    Optional<Client> findByUsername(String username);

    boolean existsByUsername(String username);

    Optional<Client> findById(Long id);

}
