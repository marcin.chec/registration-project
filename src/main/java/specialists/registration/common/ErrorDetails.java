package specialists.registration.common;

import lombok.AllArgsConstructor;

import java.util.List;

@AllArgsConstructor
public class ErrorDetails {
    public String message;
    public List<String> details;
}
