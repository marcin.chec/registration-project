package specialists.registration.common;

import specialists.registration.domain.specialist.Specialist;
import specialists.registration.domain.specialist.dto.AddedSpecialistDTO;
import specialists.registration.domain.specialist.dto.SpecialistDTO;
import specialists.registration.domain.specialist.dto.UpdatedSpecialistDTO;

public class SpecialistToDtoConverter {

    public static AddedSpecialistDTO convertSpecialistToAddedSpecialistDTO(Specialist specialist) {

        AddedSpecialistDTO addedSpecialistDTO = new AddedSpecialistDTO();

        addedSpecialistDTO.firstName = specialist.getFirstName();
        addedSpecialistDTO.lastName = specialist.getLastName();
        addedSpecialistDTO.specialization = specialist.getSpecialization();
        addedSpecialistDTO.id = specialist.getId();

        return addedSpecialistDTO;
    }

    public static UpdatedSpecialistDTO convertSpecialistToUpdatedSpecialistDTO(Specialist specialist) {

        UpdatedSpecialistDTO updatedSpecialistDTO = new UpdatedSpecialistDTO();

        updatedSpecialistDTO.firstName = specialist.getFirstName();
        updatedSpecialistDTO.lastName = specialist.getLastName();
        updatedSpecialistDTO.specialization = specialist.getSpecialization();

        return updatedSpecialistDTO;
    }

    public static SpecialistDTO convertSpecialistToSpecialistDTO(Specialist specialist) {

        SpecialistDTO specialistDTO = new SpecialistDTO();

        specialistDTO.firstName = specialist.getFirstName();
        specialistDTO.lastName = specialist.getLastName();
        specialistDTO.specialization = specialist.getSpecialization();

        return specialistDTO;
    }
}
