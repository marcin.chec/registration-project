package specialists.registration.common;

import specialists.registration.domain.registration.Registration;
import specialists.registration.domain.registration.dto.ChangedRegistrationDTO;
import specialists.registration.domain.registration.dto.MakedRegistrationDTO;

public class RegistrationToDtoConverter {

    public static MakedRegistrationDTO convertToMakedRegistrationDTO(Registration registration) {

        return new MakedRegistrationDTO(
                registration.getId(),
                registration.getClientId(),
                registration.getSpecialistId(),
                registration.getStartOfAppointment(),
                registration.getEndOfAppointment()
        );
    }

    public static ChangedRegistrationDTO convertToChangedRegistrationDTO(Registration registration) {

        return new ChangedRegistrationDTO(
                registration.getId(),
                registration.getClientId(),
                registration.getSpecialistId(),
                registration.getStartOfAppointment(),
                registration.getEndOfAppointment()
        );
    }
}
