package specialists.registration.common.exception;

public class ForeignRegistrationException extends RuntimeException {

    public ForeignRegistrationException(String message) {
        super(message);
    }
}
