package specialists.registration.common.exception;

public class ChoosenDateNotAvailableException extends RuntimeException {

    public ChoosenDateNotAvailableException(String message) {
        super(message);
    }
}
