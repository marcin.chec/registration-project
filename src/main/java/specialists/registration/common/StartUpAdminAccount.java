package specialists.registration.common;

import lombok.AllArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;
import specialists.registration.domain.client.Client;
import specialists.registration.repository.ClientRepository;

@Component
@AllArgsConstructor
public class StartUpAdminAccount implements CommandLineRunner {


    ClientRepository clientRepository;
    BCryptPasswordEncoder bCryptPasswordEncoder;

    @Override
    public void run(String... args) throws Exception {
        Client startUpAdmin = new Client(1L, "admin", bCryptPasswordEncoder.encode("admin"), "ADMIN", "admin", "admin", "admin@admin.pl");

        if (!clientRepository.existsByUsername("admin")) {
            clientRepository.saveAndFlush(startUpAdmin);
        }
    }
}
