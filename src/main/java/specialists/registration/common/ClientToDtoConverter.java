package specialists.registration.common;

import specialists.registration.domain.client.Client;
import specialists.registration.domain.client.dto.AddedClientDTO;
import specialists.registration.domain.client.dto.GetClientDTO;
import specialists.registration.domain.client.dto.UpdatedClientDTO;

public class ClientToDtoConverter {

    public static AddedClientDTO convertClientToAddedClientDTO(Client client) {
        AddedClientDTO addedClientDTO = new AddedClientDTO();

        addedClientDTO.firstName = client.getFirstName();
        addedClientDTO.lastName = client.getLastName();
        addedClientDTO.username = client.getUsername();
        addedClientDTO.email = client.getEmail();
        addedClientDTO.id = client.getId();

        return addedClientDTO;
    }

    public static UpdatedClientDTO convertClientToUpdatedClientDTO(Client client) {
        UpdatedClientDTO updatedClientDTO = new UpdatedClientDTO();

        updatedClientDTO.firstName = client.getFirstName();
        updatedClientDTO.lastName = client.getLastName();
        updatedClientDTO.username = client.getUsername();
        updatedClientDTO.email = client.getEmail();

        return updatedClientDTO;
    }

    public static GetClientDTO convertClientToDto(Client client) {
        GetClientDTO clientDTO = new GetClientDTO();

        clientDTO.firstName = client.getFirstName();
        clientDTO.lastName = client.getLastName();
        clientDTO.username = client.getUsername();
        clientDTO.email = client.getEmail();

        return clientDTO;
    }
}
