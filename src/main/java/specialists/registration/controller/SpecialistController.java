package specialists.registration.controller;

import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import specialists.registration.application.api.SpecialistService;
import specialists.registration.domain.specialist.dto.AddedSpecialistDTO;
import specialists.registration.domain.specialist.dto.SpecialistDTO;
import specialists.registration.domain.specialist.dto.UpdatedSpecialistDTO;

import javax.validation.Valid;

@RestController
@RequestMapping("specialist")
@AllArgsConstructor
@PreAuthorize("hasRole('ROLE_ADMIN')")
public class SpecialistController {

    private SpecialistService specialistService;

    @PostMapping
    public ResponseEntity<AddedSpecialistDTO> addSpecialist(@Valid @RequestBody SpecialistDTO specialistDTO) {

        return new ResponseEntity<>(specialistService.addSpecialist(specialistDTO), HttpStatus.CREATED);
    }

    @PutMapping("{id}")
    public ResponseEntity<UpdatedSpecialistDTO> updateSpecialist(@PathVariable Long id, @Valid @RequestBody SpecialistDTO specialistDTO) {

        return ResponseEntity.ok(specialistService.updateSpecialist(id, specialistDTO));
    }

    @DeleteMapping("{id}")
    public ResponseEntity<?> deleteSpecialist(@PathVariable Long id) {

        specialistService.removeSpecialist(id);
        return ResponseEntity.ok().build();
    }

    @GetMapping("{id}")
    public ResponseEntity<SpecialistDTO> getSpecialist(@PathVariable Long id) {

        return ResponseEntity.ok(specialistService.getSpecialist(id));
    }
}
