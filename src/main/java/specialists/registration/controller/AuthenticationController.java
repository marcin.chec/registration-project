package specialists.registration.controller;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import specialists.registration.common.LoginDTO;

@RestController
@RequestMapping("login")
public class AuthenticationController {

    @PostMapping
    public String login(@RequestBody LoginDTO loginDTO) {
        return "token";
    }
}
