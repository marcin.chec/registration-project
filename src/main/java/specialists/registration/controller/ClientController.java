package specialists.registration.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import specialists.registration.application.api.ClientService;
import specialists.registration.domain.client.dto.AddedClientDTO;
import specialists.registration.domain.client.dto.GetClientDTO;
import specialists.registration.domain.client.dto.NewClientDTO;
import specialists.registration.domain.client.dto.UpdatedClientDTO;

import javax.validation.Valid;

@RestController
@RequestMapping("client")
public class ClientController {

    @Autowired
    private ClientService clientService;

    @PostMapping
    public ResponseEntity<AddedClientDTO> registerClient(@Valid @RequestBody NewClientDTO newClient) {

        return new ResponseEntity<>(clientService.registerClient(newClient), HttpStatus.CREATED);
    }

    @PutMapping("{id}")
    public ResponseEntity<UpdatedClientDTO> updateClient(@Valid @RequestBody NewClientDTO updateClient, @PathVariable("id") Long id) {

        return ResponseEntity.ok(clientService.updateClient(id, updateClient));
    }

    @DeleteMapping("{id}")
    public ResponseEntity<?> deleteClient(@PathVariable("id") Long id) {

        clientService.removeClient(id);

        return ResponseEntity.ok().build();
    }

    @GetMapping("{id}")
    public ResponseEntity<GetClientDTO> getClient(@PathVariable("id") Long id) {

        return ResponseEntity.ok(clientService.getClient(id));
    }

    @GetMapping
    public ResponseEntity<GetClientDTO> getLoggedInClient() {

        return ResponseEntity.ok(clientService.getLoggedInClient());
    }
}
