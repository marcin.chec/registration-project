package specialists.registration.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import specialists.registration.application.api.RegistrationService;
import specialists.registration.domain.registration.dto.ChangedRegistrationDTO;
import specialists.registration.domain.registration.dto.MakedRegistrationDTO;
import specialists.registration.domain.registration.dto.NewRegistrationDTO;

import javax.validation.Valid;

@RestController
@RequestMapping("registration")
public class RegistrationController {

    private RegistrationService registrationService;

    public RegistrationController(RegistrationService registrationService) {
        this.registrationService = registrationService;
    }

    @PostMapping
    public ResponseEntity<MakedRegistrationDTO> doRegistration(@Valid @RequestBody NewRegistrationDTO newRegistrationDTO) {
        return ResponseEntity.ok(registrationService.doRegistration(newRegistrationDTO));
    }

    @DeleteMapping("{id}")
    public ResponseEntity<?> resignFromRegistration(@PathVariable Long id) {
        registrationService.resignFromRegistration(id);
        return ResponseEntity.ok().build();
    }

    @PutMapping("{id}")
    public ResponseEntity<ChangedRegistrationDTO> changeRegistration(@PathVariable Long id, @Valid @RequestBody NewRegistrationDTO newRegistrationDTO) {
        return ResponseEntity.ok(registrationService.changeRegistration(id, newRegistrationDTO));
    }

    @GetMapping("{id}")
    public ResponseEntity<MakedRegistrationDTO> showRegistration(@PathVariable Long id) {
        return ResponseEntity.ok(registrationService.showRegistration(id));
    }

}
