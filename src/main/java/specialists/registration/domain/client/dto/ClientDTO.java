package specialists.registration.domain.client.dto;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;

public abstract class ClientDTO {

    @NotBlank(message = "Username cannot be blank or null")
    public String username;

    @NotBlank(message = "First name cannot be blank or null")
    public String firstName;

    @NotBlank(message = "Last name cannot be blank or null")
    public String lastName;

    @NotBlank(message = "Email cannot be blank or null")
    @Email
    public String email;
}
