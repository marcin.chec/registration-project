package specialists.registration.domain.client.dto;

import javax.validation.constraints.NotBlank;

public class NewClientDTO extends ClientDTO {

    @NotBlank(message = "Password cannot be empty or null")
    public String password;

}
