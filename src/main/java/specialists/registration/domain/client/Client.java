package specialists.registration.domain.client;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Setter
@Getter
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
public class Client implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    private Long id;

    @Column(unique = true, nullable = false)
    private String username;

    @Column(nullable = false)
    private String password;

    @Column(nullable = false)
    private String permissionGroup;

    private String firstName;

    private String lastName;

    private String email;

    public Client(String username, String password, String permissionGroup, String firstName, String lastName, String email) {
        this.username = username;
        this.password = password;
        this.permissionGroup = permissionGroup;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
    }
}
