package specialists.registration.domain.specialist.dto;

public class AddedSpecialistDTO {
    public Long id;
    public String firstName;
    public String lastName;
    public String specialization;
}
