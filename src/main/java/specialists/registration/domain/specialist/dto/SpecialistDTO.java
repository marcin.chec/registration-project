package specialists.registration.domain.specialist.dto;

import javax.validation.constraints.NotBlank;

public class SpecialistDTO {

    @NotBlank(message = "First name cannot be blank or null")
    public String firstName;

    @NotBlank(message = "Last name cannot be blank or null")
    public String lastName;

    @NotBlank(message = "Specialization cannot be blank or null")
    public String specialization;
}
