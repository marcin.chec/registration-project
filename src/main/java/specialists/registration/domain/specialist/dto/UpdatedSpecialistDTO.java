package specialists.registration.domain.specialist.dto;

public class UpdatedSpecialistDTO {
    public String firstName;
    public String lastName;
    public String specialization;
}
