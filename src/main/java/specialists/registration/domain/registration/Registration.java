package specialists.registration.domain.registration;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

@Entity
@Getter
@Setter
@EqualsAndHashCode
@NoArgsConstructor
public class Registration implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(unique = true, nullable = false)
    private Long id;

    @Column(nullable = false)
    private Long clientId;

    @Column(nullable = false)
    private Long specialistId;

    @Column(name = "start_of_visit", nullable = false)
    private LocalDateTime startOfAppointment;

    @Column(name = "end_of_visit", nullable = false)
    private LocalDateTime endOfAppointment;

    public Registration(Long clientId, Long specialistId, LocalDateTime startOfAppointment, LocalDateTime endOfAppointment) {
        this.clientId = clientId;
        this.specialistId = specialistId;
        this.startOfAppointment = startOfAppointment;
        this.endOfAppointment = endOfAppointment;
    }
}
