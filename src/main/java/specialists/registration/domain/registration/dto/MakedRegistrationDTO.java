package specialists.registration.domain.registration.dto;

import lombok.AllArgsConstructor;

import java.time.LocalDateTime;

@AllArgsConstructor
public class MakedRegistrationDTO {
    public Long registrationId;
    public Long clientId;
    public Long specialistId;
    public LocalDateTime startOfAppointment;
    public LocalDateTime endOfAppointment;
}
