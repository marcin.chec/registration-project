package specialists.registration.domain.registration.dto;

import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

public class NewRegistrationDTO {

    @NotNull
    public Long specialistId;

    @NotNull
    public LocalDateTime startOfAppointment;
}
